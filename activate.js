**
 * @file Activate.js
 * @author Julian Neundorf
 * @brief This script control defines a function activating an output with toggle_after only if it wasn't already active.
 * @version 0.1
 * @date 2024-05-24
 */
 
function Activate(switchNumber, time) {
  let sw = Shelly.getComponentStatus("switch:" + switchNumber);
  if (!sw.output || typeof sw.timer_started_at !== 'undefined') {
    Shelly.call("Switch.Set", {"id": switchNumber, "on": true, "toggle_after": time});
  }
  return true;
}