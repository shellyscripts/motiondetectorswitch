/**
 * @file MotionDetector.js
 * @author Julian Neundorf
 * @brief This script control multiple outputs by a motion detector and multiple switches. A short press on a switch switch between the modes "Motion Detector" and "Always on". A long press on a switch switch between the modes "Motion Detector" and "Always off". An "external" output on another shelly is only triggered when motion is detected. FrontMotionDetected can be called to let the lights blink a single time.
 * @version 0.1
 * @date 2024-05-24
 */

let Config = {
  motionOnSeconds: 45,
  switches: [1, 2],
  motionDetector: "3",
  output: "3",
  motionDetectorTimerPeriod: 3000,
  blinkOnMilliSeconds: 500,
  numberBlinks: 4
};

let MotionDetectorLamp = {
  ipAddress: "192.168.178.168",
  scriptId: 1,
  output: 2,
  time: 45
};

let Modes = {
  Off: 0,
  Auto: 1,
  OnMotionDetected: 2,
  On: 3,
  FrontMotionDetected: 4
};
let mode = Modes.Auto;
let lastMode = mode;
let timerID = false;

print("Start");

function KillTimer(timerId) {
  if (timerId !== false) {
    Timer.clear(timerId);
    timerId = false;
  }
}

function RPCCall(command) {
  Shelly.call("HTTP.GET", { url: command });
}

// Switch internal output on/off
function SwitchOutput(on) {
  Shelly.call("Switch.Set", { "id": Config.output, "on": on });
}

// Switch output of other shelly for a certain time
function SwitchExternOutput(ip, scriptId, switchId, time) {
  RPCCall("http://" + ip + "/rpc/script.start?id=" + scriptId);
  RPCCall("http://" + ip + "/rpc/script.eval?id=" + scriptId + "&code=Activate(" + switchId + "," + time + ")");
  RPCCall("http://" + ip + "/rpc/script.stop?id=" + scriptId);
}

// Front motion detector callback function
function FrontMotionDetected() {
  lastMode = mode;
  mode = Modes.FrontMotionDetected;
  CheckMode();
}

// Using the Shelly's internal feature to switch the output for a certain time
function ActivateTimedOutput() {
  Shelly.call("Switch.Set", { "id": Config.output, "on": true, "toggle_after": Config.motionOnSeconds });
  SwitchExternOutput(MotionDetectorLamp.ipAddress, MotionDetectorLamp.scriptId, MotionDetectorLamp.output, MotionDetectorLamp.time);

  if (timerID === false) {
    timerID = Timer.set(Config.motionDetectorTimerPeriod, true,
      function (ud) {
        if (Shelly.getComponentStatus("input:" + Config.motionDetector).state === true) {
          ActivateTimedOutput();
        } else {
          mode = Modes.Auto;
          KillTimer(timerID);
          timerId = false;
        }
      },
      null
    );
  }
}

// Find value in array and return index; return false if not found
function find(array, value) {
  for (var i = 0; i < array.length; i++) {
    if (array[i] === value) {
      return i;
    }
  }
  return false;
}

// Setting mode from outside
function SetMode(mode_) {
  mode = mode_;
  CheckMode();
}

function CheckMode() {
  print("Current mode: " + mode);
  switch (mode) {
    case Modes.Off:
    case Modes.Auto:
      SwitchOutput(false);
      KillTimer(timerID);
      timerId = false;
      break;
    case Modes.OnMotionDetected:
      ActivateTimedOutput();
      break;
    case Modes.On:
      SwitchOutput(true);
      KillTimer(timerID);
      timerId = false;
      break;
    case Modes.FrontMotionDetected:
      ActivateBlinking();
      break;
  }
}

let Blink = {
  timerId: false,
  counter: 0
};

// Activate blinking of the light with JS-timer; Timer will get killed after Config.numberBlinks
function ActivateBlinking() {
  Blink.counter = 0;
  if (Blink.timerId === false) {
    lastMode = mode;
    Blink.timerId = Timer.set(Config.blinkOnMilliSeconds, true,
      function (ud) {
        if ((mode == Modes.Off || mode == Modes.FrontMotionDetected) && Blink.counter < Config.numberBlinks) {
          Shelly.call("Switch.Toggle", { "id": Config.output });
          Blink.counter = Blink.counter + 1;
        } else {
          KillTimer(Blink.timerId);
          Blink.timerId = false;
          mode = lastMode;
        }
      },
      null
    );
  }
}

Shelly.addEventHandler(function (e) {
  if (find(Config.switches, e.id) !== false) { // Switch is used
    if (e.info.event === "single_push") { // Switch On / Auto
      switch (mode) {
        case Modes.On:
          mode = Modes.Auto;
          break;
        case Modes.Off:
        case Modes.Auto:
        case Modes.OnMotionDetected:
          mode = Modes.On;
          break;
      }
      CheckMode();
    } else if (e.info.event === "long_push") { // Switch Off / Auto
      switch (mode) {
        case Modes.Off:
          mode = Modes.Auto;
          break;
        case Modes.Auto:
        case Modes.OnMotionDetected:
        case Modes.On:
          mode = Modes.Off;
          ActivateBlinking();
          break;
      }
      CheckMode();
    }
  } else if (e.component === "input:" + Config.motionDetector) { // Motion Detector
    if (e.info.event === "toggle" && e.info.state === true && mode == Modes.Auto) {
      mode = Modes.OnMotionDetected;
    }
    CheckMode();
  }
});

/*
Switch Short:
  Off -> On
  Auto -> On
  OnMotionDetected -> On
  On -> Auto

Switch Long:
  Off -> Auto
  Auto -> Off
  OnMotionDetected -> Off
  On -> Off

Motion Detector:
  Off -> Off
  Auto -> OnMotionDetected
  OnMotionDetected -> OnMotionDetected
  On -> On
  
Timer (Motion Detector == on):
  OnMotionDetected -> OnMotionDetected
  
Timer (Motion Detector == off):
  OnMotionDetected -> Auto
*/